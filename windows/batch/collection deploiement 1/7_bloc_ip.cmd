@echo off

::============================================================================
:: blocage des adresses IP via le ROUTE
::============================================================================
:: Adrersse IP avec MASK=255.255.255.255
set spy_ip=^
	2.17.21.70,2.18.126.144,2.18.126.99,2.18.245.121,2.18.245.81,2.18.245.97,2.19.224.131,2.19.225.200,2.22.61.43,2.22.61.66,^
	4.23.62.126,13.81.59.242,13.107.3.128,13.107.4.50,13.107.4.52,13.107.5.88,13.107.21.200,23.101.115.193,23.101.156.198,23.101.187.68,^
	23.102.17.214,23.102.21.4,23.103.189.125,23.103.189.126,23.193.225.197,23.193.230.88,23.193.236.70,23.193.238.90,23.193.251.132,23.206.42.56,^
	23.210.48.42,23.210.5.16,23.210.63.75,23.211.159.37,23.211.170.9,23.217.138.11,23.217.138.122,23.217.138.18,23.217.138.25,23.217.138.43,^
	23.217.138.90,23.217.138.97,23.218.212.69,23.223.20.82,23.35.118.238,23.57.101.163,23.57.107.163,23.57.107.27,23.67.60.65,23.67.60.73,^
	23.67.60.97,23.74.8.80,23.74.8.99,23.74.9.198,23.74.9.217,23.9.123.27,23.96.212.225,23.97.178.173,23.97.197.207,23.99.10.11,^
	31.13.92.2,37.252.162.217,37.252.163.144,37.252.163.145,37.252.170.141,40.113.10.78,40.113.11.93,40.117.145.132,40.122.214.188,40.69.66.208,^
	40.77.134.24,40.77.226.249,40.77.226.250,40.77.229.2,40.77.229.133,40.77.229.141,40.84.199.233,52.71.117.99,52.164.227.208,52.166.197.207,^
	52.169.118.173,52.178.167.109,64.4.6.100,64.4.11.42,64.4.54.18,64.4.54.22,64.4.54.32,64.4.54.98,64.4.54.99,64.4.54.116,64.4.54.117,^
	64.4.54.153,64.4.54.167,64.4.54.253,64.4.54.254,65.39.117.230,65.52.100.7,65.52.100.9,65.52.100.11,65.52.100.91,65.52.100.92,^
	65.52.100.93,65.52.100.94,65.52.108.3,65.52.108.27,65.52.108.29,65.52.108.33,65.52.108.92,65.52.108.94,65.52.108.103,65.52.108.153,^
	65.52.108.154,65.52.108.252,65.52.161.64,65.52.236.160,65.54.226.187,65.55.29.238,65.55.39.10,65.55.44.85,65.55.44.108,65.55.44.109,^
	65.55.57.27,65.55.83.120,65.55.108.23,65.55.113.13,65.55.128.80,65.55.128.81,65.55.130.50,65.55.138.110,65.55.138.111,65.55.138.114,^
	65.55.138.126,65.55.138.186,65.55.163.221,65.55.163.222,65.55.176.90,65.55.206.154,65.55.252.190,65.55.252.43,65.55.252.63,65.55.252.71,^
	65.55.252.92,65.55.252.93,66.119.144.157,66.119.144.158,66.119.144.189,66.119.144.190,66.119.147.131,66.119.152.204,66.119.152.205,68.232.34.200,^
	72.21.81.200,72.21.91.8,74.125.206.148,74.125.206.149,77.67.29.176,8.23.91.254,8.253.7.126,8.253.91.126,8.253.91.254,8.253.92.126,^
	8.253.92.254,8.253.93.126,8.254.93.126,8.254.94.254,8.254.96.254,8.254.173.254,8.254.209.254,8.254.214.126,8.254.215.126,8.254.215.254,^
	8.254.226.254,8.254.227.126,82.199.68.72,82.199.80.143,88.221.113.72,88.221.113.96,88.221.14.168,88.221.15.43,88.221.15.59,92.123.182.27,^
	92.123.182.58,94.245.121.176,94.245.121.177,94.245.121.178,94.245.121.179,94.245.121.251,94.245.121.253,94.245.121.254,95.101.148.186,95.101.149.158,^
	98.124.243.41,104.101.172.250,104.121.1.194,104.208.28.54,104.40.208.40,104.47.166.140,104.69.67.29,104.69.119.19,104.69.135.172,104.73.92.149,^
	104.73.138.217,104.73.143.160,104.73.153.9,104.73.160.16,104.73.160.51,104.73.160.58,104.82.14.146,104.82.22.249,104.85.17.76,104.85.38.129,^
	104.91.166.82,104.91.188.21,104.94.111.30,104.94.163.155,104.94.172.176,104.96.20.117,104.96.28.44,104.96.147.3,111.221.29.177,111.221.29.253,^
	128.63.2.53,131.107.113.238,131.107.255.255,131.253.14.121,131.253.14.153,131.253.14.76,131.253.34.240,131.253.40.109,131.253.40.37,131.253.40.53,^
	131.253.40.59,131.253.61.100,131.253.61.66,131.253.61.82,131.253.61.84,131.253.61.96,134.170.30.202,134.170.51.190,134.170.51.246,134.170.51.247,^
	134.170.51.248,134.170.51.250,134.170.52.151,134.170.53.29,134.170.53.30,134.170.58.118,134.170.58.121,134.170.58.123,134.170.58.189,134.170.58.190,^
	134.170.104.154,134.170.111.154,134.170.115.60,134.170.115.62,134.170.165.248,134.170.165.251,134.170.165.253,134.170.179.87,134.170.185.70,134.170.188.248,^
	134.170.188.84,137.116.74.190,137.116.81.24,137.117.235.16,157.55.129.21,157.55.133.204,157.55.240.220,157.56.17.248,157.56.23.91,157.56.57.5,^
	157.56.74.250,157.56.77.138,157.56.77.139,157.56.91.77,157.56.91.82,157.56.96.54,157.56.96.58,157.56.96.123,157.56.106.184,157.56.106.189,^
	157.56.121.89,157.56.124.87,157.56.144.215,157.56.144.216,157.56.149.250,157.56.194.72,157.58.211.44,157.58.249.57,161.69.13.20,161.69.17.33,^
	161.69.28.13,161.69.29.54,161.69.165.22,161.69.165.23,161.69.165.24,161.69.165.26,161.69.165.56,161.69.165.57,161.69.165.60,161.69.165.62,^
	168.61.24.141,168.62.187.13,168.63.29.74,168.63.108.233,172.217.20.38,173.194.113.219,173.194.113.220,173.194.40.123,173.194.40.124,173.223.10.103,^
	173.223.10.169,173.223.10.232,173.223.11.142,173.223.11.143,173.223.11.152,173.223.11.166,173.252.90.192,178.255.83.1,185.13.160.61,191.232.140.76,^
	191.232.80.58,191.232.80.60,191.232.80.62,191.234.72.183,191.234.72.186,191.234.72.188,191.234.72.190,191.237.208.126,192.168.1.255,192.229.233.249,^
	194.44.4.200,194.44.4.208,198.41.214.183,198.41.214.184,198.41.214.186,198.41.214.187,198.41.215.182,198.41.215.185,198.41.215.186,198.78.208.254,^
	204.79.197.209,204.79.197.210,204.79.197.211,204.79.197.213,207.123.34.126,207.123.56.252,207.46.7.252,207.46.101.29,207.46.114.58,207.46.114.61,^
	207.46.223.94,207.68.166.254,212.30.134.204,212.30.134.205,216.38.172.128,216.58.198.230,216.58.209.166,216.58.211.102,216.58.213.134,111.221.64.0,^
	23.55.155.27,23.214.171.90,64.4.11.25,65.52.100.46,88.221.113.10,88.221.113.57,95.101.148.156,184.87.182.252,198.41.214.185,198.41.215.183,^
	198.41.215.184,221.221.112.129,221.221.112.145,221.221.112.160,221.221.112.203,111.221.29.154

:: Adrersse IP avec MASK=255.255.255.0
set spy_ip2=64.4.23.0,65.55.223.0,157.55.52.0,157.55.56.0,157.55.235.0,157.55.130.0,157.55.236.0,195.138.255.0,213.199.179.0,191.232.139.2

for %%i in (%spy_ip%) do (
      IF %%i==111.221.64.0 (
      	route -p ADD %%i MASK 255.255.192.0 0.0.0.0 > nul 2>&1
     	) ELSE (
				route -p ADD %%i MASK 255.255.255.255 0.0.0.0 > nul 2>&1
			)	
)

for %%i in (%spy_ip2%) do (
			route -p ADD %%i MASK 255.255.255.0 0.0.0.0 > nul 2>&1
)

::============================================================================
:: Blocage des adresses IP via ROUTE
::============================================================================
:: Blocage Skype, MSN, MESSENGER
:: Adrersse IP avec MASK=255.255.255.255 
set spy_ipskype=^
	2.18.122.78,2.18.126.223,23.40.1.36,40.69.132.130,40.74.50.25,40.77.226.192,40.77.226.194,40.77.226.246,40.79.79.123,40.127.139.224,^
	52.169.118.173,64.4.23.151,65.54.225.167,65.55.108.23,92.122.180.48,93.184.221.200,95.101.148.83,104.71.185.14,104.85.27.201,104.94.168.220,^
	104.96.28.184,131.253.14.76,134.170.0.216,134.170.3.200,157.55.56.161,157.55.130.155,157.56.198.14,157.56.109.8,157.56.123.82,157.56.114.104,^
	157.56.194.24,207.46.11.252,207.46.194.8,207.46.194.10,207.46.194.14,207.46.194.25,207.46.194.33

for %%i in (%spy_ipskype%) do (
			route -p ADD %%i MASK 255.255.255.255 0.0.0.0 > nul 2>&1
)

::===========================================================================================================
ECHO. blocage des adresses IP via le parafeu
::===========================================================================================================
:maj_parefeu
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_01 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_01" dir=out action=block remoteip=2.17.21.70,2.18.126.144,2.18.126.99,2.18.245.121,2.18.245.81,2.18.245.97,2.19.224.131,2.19.225.200,2.22.61.43,2.22.61.66 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_02 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_02" dir=out action=block remoteip=4.23.62.126,13.81.59.242,13.107.3.128,13.107.4.50,13.107.4.52,13.107.5.88,13.107.21.200,23.101.115.193,23.101.156.198,23.101.187.68 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_03 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_03" dir=out action=block remoteip=23.102.17.214,23.102.21.4,23.103.189.125,23.103.189.126,23.193.225.197,23.193.230.88,23.193.236.70,23.193.238.90,23.193.251.132,23.206.42.56 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_04 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_04" dir=out action=block remoteip=23.210.48.42,23.210.5.16,23.210.63.75,23.211.159.37,23.211.170.9,23.217.138.11,23.217.138.122,23.217.138.18,23.217.138.25,23.217.138.43 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_05 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_05" dir=out action=block remoteip=23.217.138.90,23.217.138.97,23.218.212.69,23.223.20.82,23.35.118.238,23.57.101.163,23.57.107.163,23.57.107.27,23.67.60.65,23.67.60.73 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_06 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_06" dir=out action=block remoteip=23.67.60.97,23.74.8.80,23.74.8.99,23.74.9.198,23.74.9.217,23.9.123.27,23.96.212.225,23.97.178.173,23.97.197.207,23.99.10.11 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_07 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_07" dir=out action=block remoteip=31.13.92.2,37.252.162.217,37.252.163.144,37.252.163.145,37.252.170.141,40.113.10.78,40.113.11.93,40.117.145.132,40.122.214.188,40.69.66.208 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_08 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_08" dir=out action=block remoteip=40.77.134.24,40.77.226.249,40.77.226.250,40.77.229.2,40.77.229.133,40.77.229.141,40.84.199.233,52.71.117.99,52.164.227.208,52.166.197.207 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_09 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_09" dir=out action=block remoteip=52.169.118.173,52.178.167.109,64.4.6.100,64.4.11.42,64.4.54.18,64.4.54.22,64.4.54.32,64.4.54.98,64.4.54.99,64.4.54.116,64.4.54.117 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_10 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_10" dir=out action=block remoteip=64.4.54.153,64.4.54.167,64.4.54.253,64.4.54.254,65.39.117.230,65.52.100.7,65.52.100.9,65.52.100.11,65.52.100.91,65.52.100.92 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_11 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_11" dir=out action=block remoteip=65.52.100.93,65.52.100.94,65.52.108.3,65.52.108.27,65.52.108.29,65.52.108.33,65.52.108.92,65.52.108.94,65.52.108.103,65.52.108.153 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_12 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_12" dir=out action=block remoteip=65.52.108.154,65.52.108.252,65.52.161.64,65.52.236.160,65.54.226.187,65.55.29.238,65.55.39.10,65.55.44.85,65.55.44.108,65.55.44.109 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_13 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_13" dir=out action=block remoteip=65.55.57.27,65.55.83.120,65.55.108.23,65.55.113.13,65.55.128.80,65.55.128.81,65.55.130.50,65.55.138.110,65.55.138.111,65.55.138.114 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_14 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_14" dir=out action=block remoteip=65.55.138.126,65.55.138.186,65.55.163.221,65.55.163.222,65.55.176.90,65.55.206.154,65.55.252.190,65.55.252.43,65.55.252.63,65.55.252.71 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_15 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_15" dir=out action=block remoteip=65.55.252.92,65.55.252.93,66.119.144.157,66.119.144.158,66.119.144.189,66.119.144.190,66.119.147.131,66.119.152.204,66.119.152.205,68.232.34.200 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_16 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_16" dir=out action=block remoteip=72.21.81.200,72.21.91.8,74.125.206.148,74.125.206.149,77.67.29.176,8.23.91.254,8.253.7.126,8.253.91.126,8.253.91.254,8.253.92.126 enable=yes  localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_17 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_17" dir=out action=block remoteip=8.253.92.254,8.253.93.126,8.254.93.126,8.254.94.254,8.254.96.254,8.254.173.254,8.254.209.254,8.254.214.126,8.254.215.126,8.254.215.254 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_18/ /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_18" dir=out action=block remoteip=8.254.226.254,8.254.227.126,82.199.68.72,82.199.80.143,88.221.113.72,88.221.113.96,88.221.14.168,88.221.15.43,88.221.15.59,92.123.182.27 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_19 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_19" dir=out action=block remoteip=92.123.182.58,94.245.121.176,94.245.121.177,94.245.121.178,94.245.121.179,94.245.121.251,94.245.121.253,94.245.121.254,95.101.148.186,95.101.149.158 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_20 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_20" dir=out action=block remoteip=98.124.243.41,104.101.172.250,104.121.1.194,104.208.28.54,104.40.208.40,104.47.166.140,104.69.67.29,104.69.119.19,104.69.135.172,104.73.92.149 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_21 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_21" dir=out action=block remoteip=104.73.138.217,104.73.143.160,104.73.153.9,104.73.160.16,104.73.160.51,104.73.160.58,104.82.14.146,104.82.22.249,104.85.17.76,104.85.38.129 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_22 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_22" dir=out action=block remoteip=104.91.166.82,104.91.188.21,104.94.111.30,104.94.163.155,104.94.172.176,104.96.20.117,104.96.28.44,104.96.147.3,111.221.29.177,111.221.29.253 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_23 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_23" dir=out action=block remoteip=128.63.2.53,131.107.113.238,131.107.255.255,131.253.14.121,131.253.14.153,131.253.14.76,131.253.34.240,131.253.40.109,131.253.40.37,131.253.40.53 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_24 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_24" dir=out action=block remoteip=131.253.40.59,131.253.61.100,131.253.61.66,131.253.61.82,131.253.61.84,131.253.61.96,134.170.30.202,134.170.51.190,134.170.51.246,134.170.51.247 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_25 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_25" dir=out action=block remoteip=134.170.51.248,134.170.51.250,134.170.52.151,134.170.53.29,134.170.53.30,134.170.58.118,134.170.58.121,134.170.58.123,134.170.58.189,134.170.58.190 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_26 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_26" dir=out action=block remoteip=134.170.104.154,134.170.111.154,134.170.115.60,134.170.115.62,134.170.165.248,134.170.165.251,134.170.165.253,134.170.179.87,134.170.185.70,134.170.188.248 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_27 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_27" dir=out action=block remoteip=134.170.188.84,137.116.74.190,137.116.81.24,137.117.235.16,157.55.129.21,157.55.133.204,157.55.240.220,157.56.17.248,157.56.23.91,157.56.57.5 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_28 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_28" dir=out action=block remoteip=157.56.74.250,157.56.77.138,157.56.77.139,157.56.91.77,157.56.91.82,157.56.96.54,157.56.96.58,157.56.96.123,157.56.106.184,157.56.106.189 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_29 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_29" dir=out action=block remoteip=157.56.121.89,157.56.124.87,157.56.144.215,157.56.144.216,157.56.149.250,157.56.194.72,157.58.211.44,157.58.249.57,161.69.13.20,161.69.17.33 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_30 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_30" dir=out action=block remoteip=161.69.28.13,161.69.29.54,161.69.165.22,161.69.165.23,161.69.165.24,161.69.165.26,161.69.165.56,161.69.165.57,161.69.165.60,161.69.165.62 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_31 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_31" dir=out action=block remoteip=168.61.24.141,168.62.187.13,168.63.29.74,168.63.108.233,172.217.20.38,173.194.113.219,173.194.113.220,173.194.40.123,173.194.40.124,173.223.10.103 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_32 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_32" dir=out action=block remoteip=173.223.10.169,173.223.10.232,173.223.11.142,173.223.11.143,173.223.11.152,173.223.11.166,173.252.90.192,178.255.83.1,185.13.160.61,191.232.140.76 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_33 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_33" dir=out action=block remoteip=191.232.80.58,191.232.80.60,191.232.80.62,191.234.72.183,191.234.72.186,191.234.72.188,191.234.72.190,191.237.208.126,192.168.1.255,192.229.233.249 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_34 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_34" dir=out action=block remoteip=194.44.4.200,194.44.4.208,198.41.214.183,198.41.214.184,198.41.214.186,198.41.214.187,198.41.215.182,198.41.215.185,198.41.215.186,198.78.208.254 enable=yes localip=any protocol=any interface=any security=notrequired
)
::reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_35 /d >nul
::IF %errorlevel% EQU 1 (
:: netsh advfirewall firewall ADD rule name="telemetrie_35" dir=out action=block remoteip=199.19.71.76,199.166.0.200,201.46.194.10,204.79.197.197,204.79.197.200,204.79.197.201,204.79.197.203,204.79.197.204,204.79.197.206,204.79.197.208 enable=yes localip=any protocol=any interface=any security=notrequired
::)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_36 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_36" dir=out action=block remoteip=204.79.197.209,204.79.197.210,204.79.197.211,204.79.197.213,207.123.34.126,207.123.56.252,207.46.7.252,207.46.101.29,207.46.114.58,207.46.114.61 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_37 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_37" dir=out action=block remoteip=207.46.223.94,207.68.166.254,212.30.134.204,212.30.134.205,216.38.172.128,216.58.198.230,216.58.209.166,216.58.211.102,216.58.213.134 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_38 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_38" dir=out action=block remoteip=64.4.23.0-64.4.23.255,65.55.223.0-65.55.223.255,157.55.52.0-157.55.52.255,157.55.56.0-157.55.56.255,157.55.235.0-157.55.235.255,111.221.64.0-111.221.127.255 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_39 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_39" dir=out action=block remoteip=157.55.130.0-157.55.130.255,157.55.236.0-157.55.236.255,195.138.255.0-195.138.255.255,213.199.179.0-213.199.179.255,191.232.139.2-191.232.139.255 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_40 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_40" dir=out action=block remoteip=23.55.155.27,23.214.171.90,64.4.11.25,65.52.100.46,88.221.113.10,88.221.113.57,95.101.148.156,184.87.182.252,198.41.214.185,198.41.215.183 enable=yes localip=any protocol=any interface=any security=notrequired
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_41 /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_41" dir=out action=block remoteip=198.41.215.184,221.221.112.129,221.221.112.145,221.221.112.160,221.221.112.203 enable=yes localip=any protocol=any interface=any security=notrequired
)

:: Bloque le programme Microsoft Compatibility Appraiser
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_Comp_Appraiser /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_Comp_Appraiser" dir=out action=block program="%windir%\system32\compattelrunner.exe" enable=yes profile=private,public,domain protocol=any interface=any security=notrequired
)
:: Bloque le programme DMCLIENT
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_Microsoft_Compatibility_Appraiser /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_Microsoft_Compatibility_Appraiser" dir=out action=block program="%windir%\system32\dmclient.exe" enable=yes profile=private,public,domain protocol=any interface=any security=notrequired
)
:: Bloque le rapport d'erreur
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_Rapport_erreur /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_Rapport_erreur" dir=out action=block program="%windir%\system32\wermgr.exe" enable=yes profile=private,public,domain protocol=any interface=any security=notrequired
)
:: Bloque apps feedback
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_Feedback_Suif /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_Feedback_Suif" dir=out action=block program="%windir%\SystemApps\WindowsFeedback_cw5n1h2txyewy\FeedbackApp.Windows.exe" enable=yes profile=private,public,domain protocol=any interface=any security=notrequired
)
::Bloque T�l�m�trie
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f Windows_Telemetry /d >nul
IF %errorlevel% EQU 1 (
 REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /v "{60E6D465-398E-4850-BE86-7EF7620A2377}" /t REG_SZ /d  "v2.24|Action=Block|Active=TRUE|Dir=Out|App=C:\windows\system32\svchost.exe|Svc=DiagTrack|Name=Windows_Telemetry|" /f > NUL 2>&1
)

::Bloque Cortana
rem netsh advfirewall firewall ADD rule name="Configuration Pare-feu Cortana" dir=out action=block program="%SystemRoot%\SystemApps\Microsoft.Windows.Cortana_cw5n1h2txyewy\SearchUI.exe" enable=yes profile=private,public,domain protocol=TCP interface=any security=notrequired rmtcomputergrp=S-1-15-2-1861897761-1695161497-2927542615-642690995-327840285-2659745135-2630312742
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f Telemetrie_Cortana_et_recherche /d >nul
IF %errorlevel% EQU 1 (
REG ADD "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /v "{2765E0F4-2918-4A46-B9C9-43CDD8FCBA2B}" /t REG_SZ /d  "v2.24|Action=Block|Active=TRUE|Dir=Out|App=%SystemRoot%\SystemApps\microsoft.windows.cortana_cw5n1h2txyewy\searchui.exe|Name=Telemetrie_Cortana_et_recherche|AppPkgId=S-1-15-2-1861897761-1695161497-2927542615-642690995-327840285-2659745135-2630312742|" /f > NUL 2>&1
)

::===========================================================================================================
ECHO. Blocage des adresses IP via le parafeu pour Skype, MSN, Messenger
::===========================================================================================================
:: Si vous utilisez Skype, MSN ou Messenger mettez en commentaire les lignes suivantes
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_1_skype /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_1_skype" dir=out action=block remoteip=2.18.122.78,2.18.126.223,23.40.1.36,40.69.132.130,40.74.50.25,40.77.226.192,40.77.226.194,40.77.226.246,40.79.79.123,40.127.139.224 enable=yes localip=any protocol=any interface=any security=notrequired 
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_2_skype /d >nul
IF %errorlevel% EQU 1 (
 netsh advfirewall firewall ADD rule name="telemetrie_2_skype" dir=out action=block remoteip=52.169.118.173,64.4.23.151,65.54.225.167,65.55.108.23,92.122.180.48,93.184.221.200,95.101.148.83,104.71.185.14,104.85.27.201,104.94.168.220 enable=yes localip=any protocol=any interface=any security=notrequired 
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_3_skype /d >nul
IF %errorlevel% EQU 1 (
netsh advfirewall firewall ADD rule name="telemetrie_3_skype" dir=out action=block remoteip=104.96.28.184,131.253.14.76,134.170.0.216,134.170.3.200,157.55.56.161,157.55.130.155,157.56.198.14,157.56.109.8,157.56.123.82,157.56.114.104 enable=yes localip=any protocol=any interface=any security=notrequired 
)
reg query "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules" /f telemetrie_4_skype /d >nul
IF %errorlevel% EQU 1 (
netsh advfirewall firewall ADD rule name="telemetrie_4_skype" dir=out action=block remoteip=157.56.194.24,207.46.11.252,207.46.194.8,207.46.194.10,207.46.194.14,207.46.194.25,207.46.194.33 enable=yes localip=any protocol=any interface=any security=notrequired
)

::===========================================================================================================
ECHO Suppression des r�gles de pare-feu des logiciels espions... 
::===========================================================================================================
powershell -Command "& {Get-NetFirewallRule | Where { $_.Group -like '*@{*' } | Remove-NetFirewallRule;}"
powershell -Command "& {Get-NetFirewallRule | Where { $_.Group -eq 'DiagTrack' } | Remove-NetFirewallRule;}"
powershell -Command "& {Get-NetFirewallRule | Where { $_.DisplayGroup -eq 'Optimisation de livraison' } | Remove-NetFirewallRule;}"
powershell -Command "& {Get-NetFirewallRule | Where { $_.DisplayGroup -like 'Service de partage r�seau du Lecteur Windows Media*' } | Remove-NetFirewallRule;}"


