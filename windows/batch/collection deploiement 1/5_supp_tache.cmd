@echo off

::===========================================================================================================
ECHO. D�sactivation des t�ches programm�es
::===========================================================================================================
 SCHTASKS /query | findstr /B /I "SmartScreenSpecific" >nul && SCHTASKS /Change /TN "Microsoft\Windows\AppID\SmartScreenSpecific" /Disable >nul
 SCHTASKS /query | findstr /B /I "AitAgent" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Application Experience\AitAgent" /Disable >nul
 SCHTASKS /query | findstr /B /I "Microsoft Compatibility Appraiser" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /Disable >nul
 SCHTASKS /query | findstr /B /I "ProgramDataUpdater" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Application Experience\ProgramDataUpdater" /Disable >nul
 SCHTASKS /query | findstr /B /I "StartupAppTask" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Application Experience\StartupAppTask" /Disable >nul
 SCHTASKS /query | findstr /B /I "Proxy" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Autochk\Proxy" /Disable >nul
::REM NE PAS UTILISER AVANT SYSPREP
:: SCHTASKS /query | findstr /B /I "CreateObjectTask" >nul && SCHTASKS /Change /TN "Microsoft\Windows\CloudExperienceHost\CreateObjectTask" /Disable >nul
 SCHTASKS /query | findstr /B /I "BthSQM" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\BthSQM" /Disable >nul
 SCHTASKS /query | findstr /B /I "Consolidator" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /Disable >nul
 SCHTASKS /query | findstr /B /I "KernelCeipTask" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /Disable >nul
 SCHTASKS /query | findstr /B /I "Uploader" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Uploader" /Disable >nul
 SCHTASKS /query | findstr /B /I "UsbCeip" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /Disable >nul
 SCHTASKS /query | findstr /B /I "Microsoft-Windows-DiskDiagnosticDataColl" >nul && SCHTASKS /Change /TN "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" /Disable >nul
 SCHTASKS /query | findstr /B /I "Diagnostics" >nul && SCHTASKS /Change /TN "Microsoft\Windows\DiskFootprint\Diagnostics" /Disable >nul
 SCHTASKS /query | findstr /B /I "DmClient" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Feedback\Siuf\Dmclient" /Disable >nul
 SCHTASKS /query | findstr /B /I "File History (maintenance mode)" >nul && SCHTASKS /Change /TN "Microsoft\Windows\FileHistory\File History (maintenance mode)" /Disable >nul
 SCHTASKS /query | findstr /B /I "WinSat" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Maintenance\Winsat" /Disable >nul 
 SCHTASKS /query | findstr /B /I "GatherNetworkInfo" >nul && SCHTASKS /Change /TN "Microsoft\Windows\NetTrace\GatherNetworkInfo" /Disable >nul
 SCHTASKS /query | findstr /B /I "Sqm-Tasks" >nul && SCHTASKS /Change /TN "Microsoft\Windows\PI\Sqm-Tasks" /Disable >nul
 SCHTASKS /query | findstr /B /I "AnalyzeSystem" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem" /Disable >nul
 SCHTASKS /query | findstr /B /I "FamilySafetyMonitor" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Shell\FamilySafetyMonitor" /Disable >nul
 SCHTASKS /query | findstr /B /I "FamilySafetyRefresh" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Shell\FamilySafetyRefreshTask" /Disable >nul
 SCHTASKS /query | findstr /B /I "FamilySafetyUpload" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Shell\FamilySafetyUpload" /Disable >nul
 SCHTASKS /query | findstr /B /I "QueueReporting" >nul && SCHTASKS /Change /TN "Microsoft\Windows\Windows Error Reporting\QueueReporting" /Disable >nul

::Office 2016
:: SCHTASKS /query | findstr /B /I "AgentFallBack2016" >nul && SCHTASKS /change /TN "Microsoft\Office\OfficeTelemetry\AgentFallBack2016" /Disable
:: SCHTASKS /query | findstr /B /I "OfficeTelemetryAgentLogOn2016" >nul && SCHTASKS /Change /TN "Microsoft\Office\OfficeTelemetry\OfficeTelemetryAgentLogOn2016" /Disable
:: SCHTASKS /query | findstr /B /I "OfficeTelemetryAgentLogOn" >nul && SCHTASKS /Change /TN "Microsoft\Office\OfficeTelemetryAgentLogOn" /Disable
:: SCHTASKS /query | findstr /B /I "OfficeTelemetryAgentFallBack" >nul && SCHTASKS /Change /TN "Microsoft\Office\OfficeTelemetryAgentFallBack" /Disable
:: SCHTASKS /query | findstr /B /I "Office 15 Subscription Heartbeat" >nul && SCHTASKS /Change /TN "Microsoft\Office\Office 15 Subscription Heartbeat" /Disable

pause